const gulp = require("gulp");
const sass = require("gulp-sass");
const rename = require("gulp-rename");
const cleanCSS = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const babel = require("gulp-babel");

// compilers
function compileScss() {
  return gulp
    .src("./src/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(gulp.dest("./dist"));
}

function compileJsKind() {
  return gulp
    .src("./src/**/page-kind.js")
    .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest("./dist"));
}

function compileJsOuders() {
  return gulp
    .src("./src/**/page-ouders.js")
    .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest("./dist"));
}

// gulp functions and exports
exports.compilersForProd = gulp.parallel(
  compileScss,
  compileJsKind,
  compileJsOuders
);
