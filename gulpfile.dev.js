const gulp = require("gulp");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");
const rename = require("gulp-rename");
const browserSync = require("browser-sync").create();
const cleanCSS = require("gulp-clean-css");
const concat = require("gulp-concat");
const mustache = require("gulp-mustache");
const babel = require("gulp-babel");

// compilers
function compileTemplates() {
  return gulp
    .src("./src/templates/**/*.html")
    .pipe(mustache())
    .pipe(gulp.dest("./dist/"))
    .pipe(browserSync.stream());
}

function compileScss() {
  return (
    gulp
      .src("./src/**/*.scss")
      .pipe(sourcemaps.init())
      .pipe(sass().on("error", sass.logError))
      // .pipe(autoprefixer())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest("./dist"))
      .pipe(browserSync.stream())
  );
}

function compileJsKind() {
  return gulp
    .src("./src/**/page-kind.js")
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./dist"))
    .pipe(browserSync.stream());
}

function compileJsOuders() {
  return gulp
    .src("./src/**/page-ouders.js")
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./dist"))
    .pipe(browserSync.stream());
}

function compileImg() {
  return gulp
    .src("./src/**/*.{png,jpg,gif,svg,ico}")
    .pipe(gulp.dest("./dist/"))
    .pipe(browserSync.stream());
}

function moveData() {
  return gulp
    .src("./src/**/*.json")
    .pipe(gulp.dest("./dist"))
    .pipe(browserSync.stream());
}

// browser-sync
function bsServe() {
  browserSync.init({
    server: {
      baseDir: "./dist",
      index: "/index.html"
    },
    open: "external",
    notify: false,
    ghostMode: false
  });

  gulp.watch("./src/**/*.mustache", compileTemplates);
  gulp.watch("./src/**/*.html", compileTemplates);
  gulp.watch("./src/**/*.scss", compileScss);
  gulp.watch("./src/**/*.js", gulp.parallel(compileJsKind, compileJsOuders));
  gulp.watch("./src/**/*.json", moveData);
  gulp.watch("./src/**/*.{png,jpg,gif,svg}", compileImg);
}

// gulp functions and exports
// v - shared functions between 'Dev compile' and 'Dev serve'
const compilersForDev = gulp.parallel(
  compileTemplates,
  compileScss,
  compileJsKind,
  compileJsOuders,
  compileImg,
  moveData
);

exports.compilersForProd = gulp.parallel(
  compileTemplates,
  compileImg,
  moveData
);
exports.compilersForDevCompile = compilersForDev;
exports.compilersForDevServe = gulp.series(compilersForDev, bsServe);
