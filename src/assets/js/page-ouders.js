var storeData, selectedStore, userInput;

$("#whatsappForm").submit(function(e) {
  if (validatePostcode($("#inputPostcode").val())) {
    $("#inputPostcode").removeClass("is-invalid");
    $("#glassesSelector").css("display", "block");
    $(".loader").show();
    getStores($("#inputPostcode").val())
      .then(function(response) {
        storeData = response.data;
        appendLocation(storeData);
      })
      .catch(function(error) {
        $("#locationSelector").html("Het ophalen van de data is mislukt!");
      })
      .finally(function() {
        $(".loader").hide();
      });
  } else {
    $("#inputPostcode").addClass("is-invalid");
  }
  e.preventDefault();
});

function validatePostcode(input) {
  var re = /\b[0-9]{4} ?[a-zA-Z]{2}\b/g;
  return re.test(input);
}

var locations = [];
//
function appendLocation(stores) {
  $("#locationSelector").html("");
  locations = [];
  for (var i = 0; i < stores.length; i++) {
    let companyName = stores[i].companyName;
    let street = stores[i].street;
    let houseNumber = stores[i].houseNumber;
    let adress = stores[i].city;
    var html = `
    <div class="card" index="${i}">
    <div class="card-body">
      <h5 class="card-title">${companyName}</h5>
      <p class="card-text">
        ${street} ${houseNumber}<br>
        ${adress}
      </p>
    </div>
    </div>
    `;
    locations.push(html);
  }

  $("#locationSelector").append(locations);
}

$(document).on("click", ".card", function(e) {
  e.preventDefault();
  selectedStore = storeData[$(this).attr("index")];
  $(".card").removeClass("selected");
  $(this).addClass("selected");
  var link = generateLink(selectedStore);
  window.open(link);
});

$("#inputPostcode")
  .focusout(function() {
    $("#whatsappForm").submit();
  })
  .on("keypress", function(e) {
    if (e.which == 13) {
      $(this).blur();
    }
  });

function getStores(input) {
  var input = input.replace(/\s+/g, "");
  return axios({
    method: "get",
    url: "https://www.procornea.nl/api/verkooppunten",
    params: {
      limit: 3,
      zipcode: input.toLowerCase(),
      country: "nederland"
    }
  });
}

function generateLink(store) {
  var searchData = store.companyName + store.street + store.houseNumber;
  var url = "https://www.google.com/maps/search/" + searchData;
  return url;
}
