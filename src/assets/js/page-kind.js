var storeData, selectedStore, userInput;
var glasses = false;

function isMobileDevice() {
  return (
    typeof window.orientation !== "undefined" ||
    navigator.userAgent.indexOf("IEMobile") !== -1
  );
}

var introText =
  "Wist je dat kinderen in Nederland steeds jonger en steeds sterker bijziend worden? 😱 Ook ik loop mogelijk risico… Tijd voor actie dus! Te beginnen met een Kijk Verder Oogcheck. Hier vind je de speciale Kijk Verder optiekzaken bij ons in de buurt:";
var introTextGlasses =
  "Wist je dat een bril misschien niet het beste is voor mij? 😱 Er zijn bijvoorbeeld lenzen die kunnen zorgen dat mijn sterkte minder snel achteruit gaat! Dat is dus beter voor in de toekomst. Wil je daarom met mij een Kijk Verder Oogcheck doen? Hier vind je de speciale Kijk Verder optiekzaken bij ons in de buurt:";

$("#whatsappForm").submit(function(e) {
  if (validatePostcode($("#inputPostcode").val())) {
    $("#inputPostcode").removeClass("is-invalid");
    $("#glassesSelector").css("display", "block");
    $(".loader").show();
    getStores($("#inputPostcode").val())
      .then(function(response) {
        storeData = response.data;
        appendLocation(storeData);
      })
      .catch(function(error) {
        $("#locationSelector").html("Het ophalen van de data is mislukt!");
      })
      .finally(function() {
        $(".loader").hide();
      });
  } else {
    $("#inputPostcode").addClass("is-invalid");
  }
  e.preventDefault();
});

function validatePostcode(input) {
  var re = /\b[0-9]{4} ?[a-zA-Z]{2}\b/g;
  return re.test(input);
}

var locations = [];
//
function appendLocation(stores) {
  $("#locationSelector").html("");
  locations = [];
  for (var i = 0; i < stores.length; i++) {
    let companyName = stores[i].companyName;
    let street = stores[i].street;
    let houseNumber = stores[i].houseNumber;
    let adress = stores[i].city;
    var html = `
    <div class="card" index="${i}">
    <div class="card-body">
      <h5 class="card-title">${companyName}</h5>
      <p class="card-text">
        ${street} ${houseNumber}<br>
        ${adress}
      </p>
    </div>
    </div>
    `;
    locations.push(html);
  }

  $("#locationSelector").append(locations);
}

$(document).on("click", ".card", function() {
  selectedStore = storeData[$(this).attr("index")];
  generateMessage();
  $("#formSubmit").removeAttr("disabled");
  $(".card").removeClass("selected");
  $(this).addClass("selected");
});

$("#formSubmit").on("click", function() {
  sendText();
});

$("#inputPostcode")
  .focusout(function() {
    $("#whatsappForm").submit();
  })
  .on("keypress", function(e) {
    if (e.which == 13) {
      $(this).blur();
    }
  });

$(".switch input").on("click", function() {
  if (this.checked) {
    glasses = true;
  } else {
    glasses = false;
  }
  if (selectedStore) {
    generateMessage(glasses);
  }
});

function sendText() {
  let message = $(".formmessage > p").text();
  let textToSend = encodeURIComponent(message);
  if (isMobileDevice()) {
    window.open(`https://wa.me/?text=${textToSend}`);
  } else {
    window.open(`https://web.whatsapp.com/send?text=${textToSend}`);
  }
}

function getStores(input) {
  var input = input.replace(/\s+/g, "");

  return axios({
    method: "get",
    url: "https://www.procornea.nl/api/verkooppunten",
    params: {
      limit: 3,
      zipcode: input.toLowerCase(),
      country: "nederland"
    }
  });
}

function generateShortLink(store) {
  var searchData = store.companyName + store.street + store.houseNumber;
  var url = "https://www.google.com/maps/search/" + searchData;
  return axios({
    method: "post",
    url: "https://procornea.leau.nl/api/shorturl",
    data: {
      url: encodeURI(url)
    }
  });
}

function generateMessage(glasses) {
  generateShortLink(selectedStore)
    .then(function(response) {
      if (!glasses) {
        $(".formmessage > p").html(
          `${introText} ${response.data.link} <br> #thanksforchecking 😘`
        );
        $(".formmessage").css("display", "block");
      } else {
        $(".formmessage > p").html(
          `${introTextGlasses} ${response.data.link} <br> #thanksforchecking 😘`
        );
        $(".formmessage").css("display", "block");
      }
    })
    .catch(function(error) {
      $(".formmessage > p").html(error);
      $(".formmessage").css("display", "block");
    });
}
